#!/bin/bash
set -e

# This can't go into Dockerfile because it needs to be executed after we mount
# the volumes. :/
#
# In our Dockerfiles we install packages in development mode (pip install -e ...).
# This puts an egg link in Python packages directory. The link points to an egg-info
# directory in the project's working directory.
#
# However if we then mount an external volume under the code directory there can be
# no egg-info directories there or the directories may be out of date.
#
# If the duration of the process becomes an issue we may want to optimize it but
# it should be all right for now.
SRC=/opt/bf-sportsbook/code
pip3 install -e "$SRC/core/pylib"
pip3 install -e "$SRC/backoffice/pylib"

cd /opt/bf-sportsbook/data/
ln -s /opt/bf-sportsbook/code/backoffice/pylib/data/template template
alembic -c /opt/bf-sportsbook/code/backoffice/pylib/data/alembic.ini upgrade heads
/usr/local/bin/po_loader_permissions
app