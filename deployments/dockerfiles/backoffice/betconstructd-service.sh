#!/bin/bash
SRC=/opt/bf-sportsbook/code
pip3 install -e "$SRC/core/pylib"
pip3 install -e "$SRC/backoffice/pylib"

cd /opt/bf-sportsbook/data/
alembic -c /opt/bf-sportsbook/code/backoffice/pylib/data/alembic.ini upgrade heads
/usr/local/bin/po_loader_permissions

service supervisor start
supervisorctl start betconstructd
tail -f /var/log/supervisor/betconstructd-stderr*