FROM ubuntu:16.04

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -y \
        acl \
        build-essential \
        curl \
        dstat \
        git \
        htop \
        libpq-dev \
        libicu-dev \
        locales \
        mercurial \
        nginx \
        nmon \
        npm \
        python3-dev \
        python3-pip \
        python-dev \
        python-pip \
        rsync \
        software-properties-common \
        supervisor \
        unzip \
        vim

# Set locale
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Install pbr
RUN pip3 install pbr

ARG APP_PATH=/opt/bf-sportsbook/data
ARG SRC=/opt/bf-sportsbook/code

# Only copy what's needed before installing Python dependencies so we use Docker
# caching efficiently.
ADD bf-sportsbook/core/pylib/requirements.txt ${SRC}/core-requirements.txt
RUN pip3 install -r ${SRC}/core-requirements.txt

ADD bf-sportsbook/backoffice/pylib/requirements.txt ${SRC}/backoffice-requirements.txt
# We dirtily ignore sports-core now as it's not available yet, it'll be
# installed later
RUN /bin/bash -c 'pip3 install -r <(grep -v sports-core ${SRC}/backoffice-requirements.txt)'

# Install NPM dependencies
ADD bf-sportsbook/backoffice/http ${SRC}/backoffice-http
WORKDIR ${SRC}/backoffice-http

RUN npm cache clean -f
RUN npm install -g n
RUN n stable

RUN npm install

RUN rm -rf node_modules/dojo node_modules/dgrid
RUN mv node_modules/dojo-dstore node_modules/dstore

# Create paths
RUN mkdir -p ${APP_PATH}/caches/logs
RUN mkdir -p ${APP_PATH}/caches/GtCache
RUN mkdir -p ${APP_PATH}/http

# Move statics to /usr/http
RUN rsync -cir node_modules/ _js/
RUN rsync -cir _css _gfx index.html _js robots.txt ${APP_PATH}/http/

# Add source
ADD bf-sportsbook/core ${SRC}/core
ADD bf-sportsbook/backoffice ${SRC}/backoffice

# Add entrypoint script
ADD backoffice-entrypoint.sh /opt/bf-sportsbook/
ADD betconstructd-service.sh /opt/bf-sportsbook/
ADD services/ /etc/supervisor/conf.d/

# Cheat pbr which fails if it has no acces to the repo while trying to pull
# revision.
ENV PBR_VERSION "0.0.1docker"
ENV GTOOL_ENVPATH ${APP_PATH}

# Install core package
RUN pip3 install -e ${SRC}/core/pylib

# Install backoffice package
RUN pip3 install -e ${SRC}/backoffice/pylib

# Add necessary configs
ADD default-docker.json ${APP_PATH}/config/default-docker.json
RUN rsync -cir ${SRC}/core/pylib/data/config/ ${APP_PATH}/config/
RUN rsync -cir ${SRC}/backoffice/pylib/data/config/ ${APP_PATH}/config/

# Add scripts to /usr/local/bin
ENV PATH=${SRC}/core/pylib/data/bin/:${SRC}/backoffice/pylib/data/bin/:${PATH}

EXPOSE 8080

# We set workdir to $SRC/backoffice-http earlier, in case we mount something under
# $SRC we need to make sure that the final working directory actually exists,
# otherwise containers using this image won't start.
WORKDIR "${SRC}"
VOLUME "${SRC}"

ENTRYPOINT ["/opt/bf-sportsbook/backoffice-entrypoint.sh"]
