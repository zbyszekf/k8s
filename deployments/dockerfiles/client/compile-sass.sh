#!/bin/bash

SASS_PATH="$1"

if [ -d "$1" ]
then
    cd $1
    compass compile
fi