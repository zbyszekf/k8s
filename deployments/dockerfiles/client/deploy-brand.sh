#!/bin/bash

BRAND_NAME="$1"
APP_PATH="$2"

if [ "$brand" != "default" ]
then
    echo "Deploying brand: $BRAND_NAME"
    # rsync -cir /opt/bf-sportsbook/code/client/pylib/data/brands/${BRAND_NAME}/http/ ${APP_PATH}/http/
    rsync -cir /opt/bf-sportsbook/code/client/pylib/data/brands/${BRAND_NAME}/config ${APP_PATH}/
    # rsync -cir /opt/bf-sportsbook/code/client/pylib/data/brands/${BRAND_NAME}/locale ${APP_PATH}/
fi