def generate_config(values, prefix='', name_delimiter='__'):
    result = {}

    for key in (x for x in values.keys() if x.startswith(prefix)):
        sub_keys = key.split(name_delimiter)
        sub_dict = result

        for i in sub_keys[1:-1]:
            if sub_dict.get(i) is None:
                sub_dict[i] = {}
            sub_dict = sub_dict[i]

        sub_dict[sub_keys[-1]] = values[key]

    return result


class FilterModule(object):
    ''' Custom filters are loaded by FilterModule objects '''

    def filters(self):
        ''' FilterModule objects return a dict mapping filter names to
            filter functions. '''
        return {
            'generate_config': generate_config,
        }
